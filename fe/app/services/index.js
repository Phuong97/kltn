import RestClient from './RestClient';
import ProductService from './ProductService';

export const productService = new ProductService(new RestClient());
