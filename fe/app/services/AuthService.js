export default class AuthService {
  constructor(restClient) {
    this.restClient = restClient;
  }

  signIn = (username, password) =>
    this.restClient.post(`/authenticate`, {
      username,
      password,
    });

  loginInfo = () => this.restClient.get('/login-info');
}
