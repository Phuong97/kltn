const ENTRY_POINT = 'product';
export default class ProductService {
  constructor(restClient) {
    this.restClient = restClient;
  }

  getAllProduct = () => this.restClient.get(`${ENTRY_POINT}`);

  getDetailProduct = productId =>
    this.restClient.get(`${ENTRY_POINT}/${productId}`);

}
