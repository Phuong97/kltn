import axios from 'axios';

export const statusCode = {
  OK: 200,
  CREATED: 201,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  INTERNAL_SERVER_ERROR: 500,
};

const DEFAULT_CONFIG = {
  baseURL: '/api',
  headers: {
    'Content-Type': 'application/json',
  },
  params: {},
  data: {},
};

export default class RestClient {
  // , isAlwaysReloadToken = true
  constructor(config = {}) {
    this.config = { ...DEFAULT_CONFIG, ...config };
    // this.isAlwaysReloadToken = isAlwaysReloadToken;
  }

  reloadConfigFromLocalStorege() {
    const token = localStorage.getItem('accessToken');
    this.config.headers.Authorization = `Bearer ${token}`;
  }

  get(url, params = {}, config = {}) {
    return this.executeRequest(url, { ...config, params });
  }

  post(url, data = {}, params = {}, config = {}) {
    return this.executeRequest(url, {
      method: 'POST',
      data,
      params,
      ...config,
    });
  }

  put(url, data = {}, params = {}, config = {}) {
    return this.executeRequest(url, {
      method: 'PUT',
      data,
      params,
      ...config,
    });
  }

  delete(url, data = {}, params = {}) {
    return this.executeRequest(url, {
      method: 'DELETE',
      data,
      params,
    });
  }

  executeRequest(url, config = {}) {
    // if (this.isAlwaysReloadToken) this.reloadConfigFromLocalStorege();
    const finalConfig = { ...this.config, ...{ url, ...config } };
    return axios
      .request(finalConfig)
      .then(response => Promise.resolve(response.data))
      .catch(errorResponse => {
        const { status } = errorResponse.response;
        const error = errorResponse.response.data;

        if (status === statusCode.UNAUTHORIZED) {
          // TODO
          // store.dispatch(unauthorize());
        }

        if (status === statusCode.NOT_FOUND) {
          // TODO
          // store.dispatch(push('/not-found'));
        }

        if (status === statusCode.INTERNAL_SERVER_ERROR) {
          // TODO
          // store.dispatch(push('/error'));
        }

        // eslint-disable-next-line prefer-promise-reject-errors
        return Promise.reject({ status, ...error });
      });
  }
}
