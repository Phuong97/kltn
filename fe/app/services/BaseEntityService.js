export default class BaseEntityService {
  constructor(restClient, entryPoint) {
    this.restClient = restClient;
    this.entryPoint = entryPoint;
  }

  getAll = () => this.restClient.get(`${this.entryPoint}?size=1000`);

  get = entityId => this.restClient.get(`${this.entryPoint}/${entityId}`);

  create = newEntity => this.restClient.post(this.entryPoint, newEntity);

  update = (entityId, updatedEntity) =>
    this.restClient.put(`${this.entryPoint}/${entityId}`, updatedEntity);

  delete = entityId => this.restClient.delete(`${this.entryPoint}/${entityId}`);
}
