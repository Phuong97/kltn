import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectAdminDomain = state => state.homePage || initialState;

const makeSelectAdmin = () =>
  createSelector(
    selectAdminDomain,
    adminState => adminState,
  );

export default makeSelectAdmin;
export { selectAdminDomain };
