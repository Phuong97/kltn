import produce from 'immer';
import { GET_ALL_PRODUCT, GET_ALL_PRODUCT_SUCCESS } from './constants';

// The initial state of the App
export const initialState = {
  isLoading: false,
  product: {},
};



const adminReducer = (state = initialState, action={}) => 
{
  switch (action.type) {
    case GET_ALL_PRODUCT:
      return {
        ...state,
        isLoading: true,
      }
    case GET_ALL_PRODUCT_SUCCESS:
      return {
        ...state, 
        isLoading: false,
        product: action.product
      }
    default:
      return state;
  }
};



export default adminReducer;
