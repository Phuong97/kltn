/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectSaga from '../../utils/injectSaga';
import injectReducer from '../../utils/injectReducer';

import makeSelectAdmin from './selectors';
import reducer from './reducer';
import saga from './saga';

import AppBar from './component/appBar';
import MenuLeft from './component/menuLeft';
import { getAllProductAction } from './actions';

const key = 'home';

export class HomePage extends React.PureComponent {

  componentDidMount = () => {
    this.props.getAllProduct();
  };

  state = {
    left: false,
  };

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  render() {
    console.log(this.props);
    return (
    <React.Fragment>
      <AppBar toggleDrawer={this.toggleDrawer} history={this.props.history} />
      <MenuLeft toggleDrawer={this.toggleDrawer} open={this.state.left} />
    </React.Fragment>
    );
  }
}

HomePage.propTypes = {

};

export const mapStateToProps = createStructuredSelector({
  admin: makeSelectAdmin(),
});

export const mapDispatchToProps = (dispatch) => ({
  dispatch,
  getAllProduct: () => dispatch(getAllProductAction()),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'homePage', reducer });
const withSaga = injectSaga({ key: 'homePage', saga });

export default compose(withConnect, withReducer, withSaga)(HomePage);
