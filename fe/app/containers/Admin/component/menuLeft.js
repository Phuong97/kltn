import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';

const styles = {
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
};

export class MenuLeft extends React.PureComponent {
  state = {
    open: this.props.open || false,
  };

  static getDerivedStateFromProps = (props, state) => {
    if (props.open !== state.open) {
      return {
        open: props.open,
      };
    }
    return null;
  };

  render() {
    const { classes, toggleDrawer } = this.props;  
    const { open } = this.state;
    const sideList = (
      <div className={classes.list}>
        <List>
          {['Product', 'Order', 'User'].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <InboxIcon />}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
      </div>
    );

    return (
      <div>
        <Drawer open={open} onClose={toggleDrawer('left', false)}>
          <div
            tabIndex={0}
            role="button"
            onClick={toggleDrawer('left', false)}
            onKeyDown={toggleDrawer('left', false)}
          >
            {sideList}
          </div>
        </Drawer>
      </div>
    )
    }
}

MenuLeft.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MenuLeft);
