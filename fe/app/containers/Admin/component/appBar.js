import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

export class AppBarComponent extends React.PureComponent {
  
  login = () => this.props.history.push('/login');

  render() {
  const { classes, toggleDrawer } = this.props;
  return(
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton 
            className={classes.menuButton}
            color="inherit" aria-label="Menu" 
            onClick={toggleDrawer('left', true)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" color="inherit" className={classes.grow}>
            News
          </Typography>
          <Button color="inherit" onClick={this.login}>Login</Button>
        </Toolbar>
      </AppBar>
    </div>
  )
  }
}

AppBarComponent.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AppBarComponent);