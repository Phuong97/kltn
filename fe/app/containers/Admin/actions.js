import { GET_ALL_PRODUCT, GET_ALL_PRODUCT_SUCCESS } from './constants';

export const getAllProductAction = () => ({
  type: GET_ALL_PRODUCT,
});

export const getAllProductSuccessAction = product => ({
  type: GET_ALL_PRODUCT_SUCCESS,
  product,
});