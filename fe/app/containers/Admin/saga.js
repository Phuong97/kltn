import { takeEvery, put, all } from 'redux-saga/effects';
import { productService } from '../../services';

import { getAllProductSuccessAction } from './actions';
import { GET_ALL_PRODUCT } from './constants';

export function* getAllProduct() {
  try {
    const product = yield productService.getAllProduct();
    yield put(getAllProductSuccessAction(product));
  } catch (err) {
    console.log(err);
  }
}

export function* watchGetAllProduct() {
  yield takeEvery(GET_ALL_PRODUCT, getAllProduct);
}

export default function* rootSaga() {
  yield all([watchGetAllProduct()]);
}
