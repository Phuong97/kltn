var mongoose = require('mongoose'),
User = mongoose.model('User');

exports.listAllUser = function(req, res) {
  User.find({}, function(err, user) {
    if (err) 
      res.send(err);
    res.json(user);
  })
}

exports.addUser = function(req, res) {
  var newUser = new User(req.body);
  newUser.save(function(err, user) {
    if(err)
      res.send(err);
    res.json(user);
  })
}

exports.findUser = function(req, res) {
  User.findById(req.params.userId, function(err, user) {
    if(err) 
      res.send(err);
    res.json(user);
  })
}