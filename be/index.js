var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var cors = require('cors')
var morgan = require('morgan')

const app = express();

var mongoDB = 'mongodb://127.0.0.1/kltn';
mongoose.connect(mongoDB, { useNewUrlParser: true });

// var db = mongoose.connect();
// db.on('err', console.error.bind(console, 'MongoDB connection error'));

//import 
const userRouter = require('./src/router/userRouter');
const productRouter = require('./src/router/productRouter');
const orderRouter = require('./src/router/orderRouter');

//middleware
app.use(cors());
app.use(morgan('dev'));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Route
app.use('/api/user', userRouter);
app.use('/api/product/', productRouter);
app.use('/api/order', orderRouter);

// Catch error
app.use((req, res, next) => {
  const err = new Error('Not found');
  err.status = 404;
  next(err);
})

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({
    err: {
      message:err.message
    }
  })
})

//Run
port = process.env.PORT || 3001;
app.listen(port);
console.log('todo list RESTful API server started on: ' + port);